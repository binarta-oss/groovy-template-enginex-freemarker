package binarta.groovyx.freemarker

import freemarker.template.Configuration
import groovy.text.Template
import groovy.transform.CompileStatic

import static java.util.Collections.emptyMap

@CompileStatic
class FreemarkerTemplate implements Template {
    private Configuration cfg

    @Override
    Writable make() {
        make(emptyMap())
    }

    @Override
    Writable make(Map binding) {
        new FreemarkerWritable(cfg:cfg, binding:binding)
    }
}
