package binarta.groovyx.freemarker

import freemarker.template.Configuration
import groovy.transform.CompileStatic

@CompileStatic
class FreemarkerWritable implements Writable {
    private Configuration cfg
    private Map binding

    @Override
    Writer writeTo(Writer out) throws IOException {
        def template = cfg.getTemplate('-')
        template.process(binding, out)
        return out
    }
}
