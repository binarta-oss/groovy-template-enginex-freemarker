package binarta.groovyx.freemarker

import freemarker.cache.NullCacheStorage
import freemarker.template.Configuration
import freemarker.template.TemplateDirectiveModel
import groovy.text.Template
import groovy.text.TemplateEngine
import org.codehaus.groovy.control.CompilationFailedException

class FreemarkerTemplateEngine extends TemplateEngine {
    private Configuration cfg = new Configuration(freemarker.template.Configuration.VERSION_2_3_23)
    private static ThreadLocal<Reader> l = new ThreadLocal<Reader>()

    static void setReader(Reader reader) {
        l.set(reader)
    }

    static Reader getReader() {
        l.get()
    }

    FreemarkerTemplateEngine() {
        cfg.templateLoader = new FreemarkerTemplateLoader()
        cfg.defaultEncoding = 'UTF-8'
        cfg.templateExceptionHandler = freemarker.template.TemplateExceptionHandler.RETHROW_HANDLER
        cfg.logTemplateExceptions = false
        cfg.cacheStorage = new NullCacheStorage()
    }

    @Override
    Template createTemplate(Reader reader) throws CompilationFailedException, ClassNotFoundException, IOException {
        FreemarkerTemplateEngine.reader = reader
        new FreemarkerTemplate(cfg: cfg)
    }

    void put(String key, TemplateDirectiveModel directive) {
        cfg.setSharedVariable(key, directive)
    }
}
